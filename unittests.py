import unittest
import re
import word_ladder

class TestInput(unittest.TestCase):
    """Test different user inputs"""

    def test_input(self):
        """Does user input dictionary name as required?"""
        expectation = "dictionary.txt"
        result = word_ladder.fname
        self.assertEqual(expectation, result)

class TestSame(unittest.TestCase):
    """Tests for function same()"""
    
    def test_same(self):
        """Does same() returns number of similar letters?"""
        expectation = 3
        result = word_ladder.same('lead', 'load')
        self.assertEqual(expectation, result) 

class TestBuild(unittest.TestCase):
    """Tests for function build()""" 
    
    def test_build(self):
        """Does build() returns list of similar words with curtain pattern?"""  
        word_ladder.words = ['bead', 'beak', 'dead', 'deaf', 'head', 'heal', 'mead', 'meat']  
        word_ladder.seen = {}   
        word_ladder.list = [] 
        word_ladder.ban = []
        expectation = ['bead', 'dead', 'head', 'mead']
        result = word_ladder.build(".ead", word_ladder.words, word_ladder.seen, word_ladder.list)
        self.assertEqual(expectation, result)
    
    def test_build_seen(self):
        """Does build() returns list of similar words, when word is already in seen?"""  
        word_ladder.words = ['bead', 'beak', 'dead', 'head', 'heal', 'mead', 'meat', 'read']  
        word_ladder.seen = {'dead': True}   
        word_ladder.list = [] 
        word_ladder.ban = []
        expectation = ['bead', 'head', 'mead', 'read']
        result = word_ladder.build(".ead", word_ladder.words, word_ladder.seen, word_ladder.list)
        self.assertEqual(expectation, result)
        
    def test_build_list(self):
        """Does build() returns list of similar words, when word is already in list?"""  
        word_ladder.words = ['bead', 'beak', 'dead', 'head', 'heal', 'mead', 'meat', 'read']  
        word_ladder.seen = {}   
        word_ladder.list = ['read'] 
        word_ladder.ban = []
        expectation = ['bead', 'dead', 'head', 'mead']
        result = word_ladder.build(".ead", word_ladder.words, word_ladder.seen, word_ladder.list)
        self.assertEqual(expectation, result)
        
    def test_build_ban(self):
        """Does build() returns list of similar words, when word is banned?"""  
        word_ladder.words = ['bead', 'beak', 'dead', 'head', 'heal', 'mead', 'meat', 'read']  
        word_ladder.seen = {}   
        word_ladder.list = [] 
        word_ladder.ban = ['bead']
        expectation = ['dead', 'head', 'mead', 'read']
        result = word_ladder.build(".ead", word_ladder.words, word_ladder.seen, word_ladder.list)
        self.assertEqual(expectation, result)
        
    def test_build_bans(self):
        """Does build() returns list of similar words, when words are banned?"""  
        word_ladder.words = ['bead', 'beak', 'dead', 'head', 'heal', 'mead', 'meat', 'read']  
        word_ladder.seen = {}   
        word_ladder.list = [] 
        word_ladder.ban = ['bead', 'head']
        expectation = ['dead', 'mead', 'read']
        result = word_ladder.build(".ead", word_ladder.words, word_ladder.seen, word_ladder.list)
        self.assertEqual(expectation, result)

class TestFind(unittest.TestCase):
    """Tests for function find()""" 
    
    def test_find(self):
        """Does find() returns words?""" 
        word_ladder.word = "lead"
        word_ladder.target = "goad"
        word_ladder.path = ['lead']
        word_ladder.d_to_target = 0
        word_ladder.words = ['bead', 'dead', 'head', 'mead', 'read', 'load', 'lend']  
        word_ladder.seen = {}   
        word_ladder.list = [] 
        result = word_ladder.find(word_ladder.word, word_ladder.words, word_ladder.seen, word_ladder.target, word_ladder.path, word_ladder.d_to_target)
        self.assertTrue(result)
        
    def test_find_path(self):
        """Does find() returns words for path?""" 
        word_ladder.word = "lead"
        word_ladder.target = "goad"
        word_ladder.path = ['lead']
        word_ladder.d_to_target = 0
        word_ladder.words = ['bead', 'dead', 'head', 'mead', 'read', 'load', 'lend']  
        word_ladder.seen = {}   
        word_ladder.list = [] 
        result = word_ladder.find(word_ladder.word, word_ladder.words, word_ladder.seen, word_ladder.target, word_ladder.path, word_ladder.d_to_target)
        expectation = ['lead', 'load']
        self.assertEqual(expectation, word_ladder.path)
        
if __name__ == '__main__':
    unittest.main()
    
