import re
import os

# recognise the letter similarity between current item and target
def same(item, target):
    return len([c for (c, t) in zip(item, target) if c == t])

# find words that matches given one by curtain pattern and not being used, not banned and
    # not already in this list 
def build(pattern, words, seen, list):
    return [word for word in words
                if re.search(pattern, word) and word not in seen.keys() and
                word not in list and word not in ban]

# find the path from start to target words
def find(word, words, seen, target, path, d_to_target):
  list = []
  # for every letter in word
  for i in range(len(word)):
    # make a list of all the words which has only one letter difference
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  # if there is no words in list, return False
  if len(list) == 0:
    return False
  # sort this list by number of letters which are exactly same as in given word
  list = sorted([(same(w, target), w) for w in list], reverse = True)
  # for every number in sorted list and word
  for (match, item) in list:
    # get out of iteration branch
    # if number of matches same as word - 1 letter, record path step
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    # put word used for search in seen list (already been used for search words)
    seen[item] = True
  # continue next step of iteration
  for (match, item) in list:
    path.append(item)
    # check does the range is less than match number. If yes >> take this word as a next one 
        # for search and search again
    if d_to_target < match:
        d_to_target = match
        if find(item, words, seen, target, path, d_to_target):
            return True
    # if range is equal match, find() return true >> path found
    elif d_to_target == match:
        if find(item, words, seen, target, path, d_to_target):
           return True
        # else exclude last step
        else:
            path.pop()
    # else exclude one step and abort
    else:
        path.pop()
        return False

# main function part

# loop for checking if file name is right
while True:
    try:
        # ask user for dictionary file
        fname = input("Enter dictionary name: ")
        #try to open file with given name
        file = open(fname)
    # if file not found >> print error message and ask user to input again    
    except IOError:
        # if input has .txt extension, but not found
        if fname.endswith('.txt') and fname != "dictionary":
           print("Sorry, it's need to be dictionary.txt, not " + fname)
        # if user input file name without extension
        else: 
           print("Sorry, file need to have an .txt extension")
        continue   
    else:
        # if file is not empty >> read lines
        if os.stat(fname).st_size > 0:
         lines = file.readlines()
         break
        else:
         print("Sorry, dictionary can't be empty") 
while True:
  # ask user for start word
  start = input("Enter start word:")
  # check that user input for start word has no digits
  while start.isalpha() != True:
    start = input("Sorry, you can use letters without numbers only:")
    # check that user input only one start word
    while not re.match(r'\A[\w-]+\Z', start): 
      start = input("Sorry, you need to enter only one word(no spaces allowed):")
  # make sure that start word is in lower case
  start = start.lower()    
  # list of words from dictionary of the same length as start word
  words = []
  for line in lines:
    word = line.rstrip()
    if len(word) == len(start):
      words.append(word)
  # ask user for target word    
  target = input("Enter target word:")
  # check that user target word with same length as start word
  while len(target) != len(start):
    target = input("Sorry, target word need to be same length as start one:")
  # check that user input for target word has no digits
  while target.isalpha() != True:
    target = input("Sorry, you can use letters without numbers only:")
    # check that user input only one target word
    while not re.match(r'\A[\w-]+\Z', target):
      target = input("Sorry, you need to enter only one word(no spaces allowed):")
  # make sure that target word is in lower case    
  target = target.lower()
  # one more user input for words that can't be used in chain start >> target
  ban = input("Enter word which can't be used:")
  # list of separated banned words from user input in lower case 
  ban_list = (ban.lower()).split()
  # prevent user from banning start/target word
  while start in ban_list or target in ban_list:
    ban = input("Sorry, start or target words can't be banned! Try again:")
    # list of separated banned words from user input in lower case
    ban_list = (ban.lower()).split()
  # make sure that target words are in lower case
  ban = ban.lower()
  break

count = 0
# begin path with start word
path = [start]
seen = {start : True}
#setting the initial similarity(distance) to target
d_to_target = 0
# if find() function returns true
if find(start, words, seen, target, path, d_to_target):
  # add target word at the end of the path and print length of the path and path
  path.append(target)
  print(len(path) - 1, path)
  print('All banned words: ' + ban)
else:
  print("No path found")

